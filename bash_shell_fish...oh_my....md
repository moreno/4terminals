# Bash, Shell, Fish *...oh my ?!*
---------------------------------------------------
I do not own or participate in these projects.  
See the links below to look for the contributors.  
G-od bless 'em.

# 1. Zsh 
sources + infos => [https://www.zsh.org/]( https://www.zsh.org/)  

## via curl

``` sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"``` 

## via wget
``` sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"``` 

# 2. oh my bash : 
sources + infos => [https://ohmybash.nntoan.com/](https://ohmybash.nntoan.com/)

## via curl
```  $bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"``` 

## via wget
``` $bash -c "$(wget https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh -O -)"```

# 3. fish shell debian 11:  
sources + infos => [https://fishshell.com/](https://fishshell.com/)

``` echo 'deb http://download.opensuse.org/repositories/shells:/fish:/release:/3/Debian_11/ /' | sudo tee /etc/apt/sources.list.d/shells:fish:release:3.list``` 

``` curl -fsSL https://download.opensuse.org/repositories/shells:fish:release:3/Debian_11/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/shells_fish_release_3.gpg > /dev/null```

``` sudo apt update```  
``` sudo apt install fish ``` 

# 4. oh my fish : 
sources + infos =>[https://github.com/oh-my-fish/oh-my-fish](https://github.com/oh-my-fish/oh-my-fish)

## via curl 

```curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish``` 

or  

```curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install > install fish install --path=~/.local/share/omf --config=~/.config/omf```




 

